/** @typedef { import("./types").Ayah } Ayah */
/** @typedef { import("./types").Data } Data */
/** @typedef { import("./types").ChaptersEndpoint } ChaptersEndpoint */
/** @typedef { import("./types").JuzEndpoint } JuzEndpoint */
/** @typedef { import("./types").Pagination } Pagination */
/** @typedef { import("./types").RevelationTypes } RevelationTypes */
/** @typedef { import("./types").Surah } Surah */
/** @typedef { import("./types").VerseByPage } VerseByPage */
/** @typedef { import("@elderjs/elderjs").HookOptions } HookOptions */
/** @typedef { { data: Data } } HookRunParam */

const path = require( 'path' );
const glob = require( 'glob' );
const fs = require( 'fs-extra' );
const flat_cache = require( 'flat-cache' );
const fetch = require( 'node-fetch' ).default;

const API_URL = 'https://api.quran.com/api/v4';
const CACHE_DIR = path.resolve( process.cwd(), '.cache' );
const TOTAL_PAGES = 604;
const PAGE_NUMBERS = Array.from( { length: TOTAL_PAGES }, ( _, i ) => i + 1 );
/** @type {RevelationTypes} */
const REVELATION_TYPES = {
	madinah: 'مدنية',
	makkah: 'مكية',
};

/**
 * Fetch data from specified URL
 *
 * @param {string} url URL to fetch.
 * @return {Object} data.
 */
function fetch_data( url ) {
	return (
		fetch( url )
			.then( response => response.json() )
			// eslint-disable-next-line no-console
			.catch( e => console.log( e ) )
	);
}

/**
 * Get all surahs fron API
 *
 * @param {string} api_path API path.
 * @param {string} cache_id Cache ID.
 * @return {Promise<any>} Array of surahs data.
 */
async function fetch_api_data( api_path, cache_id ) {
	const cache = flat_cache.load( cache_id, CACHE_DIR );
	const cache_key = 'data';

	let response = cache.getKey( cache_key );

	if ( response ) {
		return response;
	}

	response = await fetch_data( API_URL + api_path );

	cache.setKey( cache_key, response );
	cache.save( true );

	return response;
}

/**
 * Create fetch_page hook
 *
 * @param {number} page_number Page number.
 * @return {HookOptions} Hook object to fetch page data.
 */
function create_fetch_page_hook( page_number ) {
	return {
		hook: 'bootstrap',
		name: `fetch_page__${ page_number }`,
		description: 'Fetch page data',
		priority: 60,
		/**
		 * @param {HookRunParam} arg Route arguments.
		 * @return {Promise<HookRunParam>} Object with amended data.
		 */
		run: async ( { data } ) => {
			const { ayahs = [], pages = [], surahs } = data;

			/** @type {VerseByPage} */
			const api_data = await fetch_api_data(
				`/verses/by_page/${ page_number }?per_page=50&fields=chapter_id,text_uthmani,v1_page&page=1`,
				`quran.com-page-${ page_number }`,
			);

			const { chapter_id } = api_data.verses.find( a => a.verse_number === 1 ) || api_data.verses[ 0 ];
			const page_surahs = surahs
				.filter( s => page_number >= s.pages[ 0 ] && page_number <= s.pages[ 1 ] )
				.map( s => s.id );

			return {
				data: {
					...data,
					ayahs: ayahs.concat( api_data.verses ),
					pages: pages.concat( [
						{
							id: page_number,
							ayahs: api_data.verses.map( a => a.id ),
							surahs: page_surahs,
							surah_of_page: chapter_id,
						},
					] ),
				},
			};
		},
	};
}

/**
 *
 * @param {Surah} surah Surah data from API
 * @return {Surah} Complete surah data.
 */
function add_surah_data( surah ) {
	return {
		...surah,
		revelation_text: REVELATION_TYPES[ surah.revelation_place ],
	};
}

/** @type {HookOptions[]}  */
const hooks = [
	{
		hook: 'bootstrap',
		name: 'copyAssetsToPublic',
		priority: 99,
		description:
			'Copies ./assets/ to the "distDir" defined in the elder.config.js. This function helps support the live reload process.',
		run: ( { settings } ) => {
			// note that this function doesn't manipulate any props or return anything.
			// It is just executed on the 'bootstrap' hook which runs once when Elder.js is starting.

			/** @param {string} file File path. */
			const copy_file = file => {
				const parsed = path.parse( file );
				// Only write the file/folder structure if it has an extension
				if ( parsed.ext && parsed.ext.length > 0 ) {
					const relativeToAssetsFolder = path.relative( path.join( settings.rootDir, './assets' ), file );
					const outputPath = path.resolve( settings.distDir, relativeToAssetsFolder );
					fs.ensureDirSync( path.parse( outputPath ).dir );
					fs.outputFileSync( outputPath, fs.readFileSync( file ) );
				}
			};

			// copy assets folder to public destination
			glob.sync( path.resolve( settings.rootDir, './assets/**/*' ) ).forEach( copy_file );
		},
	},
	{
		hook: 'bootstrap',
		name: 'add_constants',
		description: 'Add useful constants.',
		priority: 99,
		run: ( { data } ) => ( {
			data: {
				...data,
				PAGE_NUMBERS,
				REVELATION_TYPES,
				TOTAL_PAGES,
			},
		} ),
	},
	{
		hook: 'bootstrap',
		name: 'fetch_juzs',
		description: 'Fetch juzs from Quran.com API.',
		priority: 61,
		run: async ( { data } ) => {
			/** @type {JuzEndpoint} */
			const api_data = await fetch_api_data( '/juzs', 'quran.com-juzs' );

			return {
				data: {
					...data,
					juzs: api_data.juzs,
				},
			};
		},
	},
	{
		hook: 'bootstrap',
		name: 'fetch_surahs',
		description: 'Fetch surahs from Quran.com API.',
		priority: 61,
		run: async ( { data } ) => {
			/** @type {ChaptersEndpoint} */
			const api_data = await fetch_api_data( '/chapters', 'quran.com-surahs' );
			/** @type {Surah[]} */
			const surahs = api_data.chapters.map( add_surah_data );

			return {
				data: {
					...data,
					surahs,
				},
			};
		},
	},
	...PAGE_NUMBERS.map( create_fetch_page_hook ),
	{
		hook: 'stacks',
		name: 'setHtmlAttributes',
		description: 'Set HTML lang attribute.',
		priority: 40,
		run: ( { htmlAttributesStack, settings } ) => {
			return {
				htmlAttributesStack: [
					...htmlAttributesStack,
					{
						source: 'quransetHtmlAttributes',
						string: `dir="rtl" lang="${ settings.lang }"`,
					},
				],
			};
		},
	},
];

module.exports = hooks;
