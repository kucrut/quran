import { withLocalStorage } from './with-localstorage';

/**
 * Create bookmarks store
 *
 * @return {import("../types").BookmarkStore} Bookmark store.
 */
export function createBookmarks() {
	const store = withLocalStorage( 'bookmarks', {
		last_open: 0,
		pages: [],
	} );

	return {
		...store,

		/**
		 * Add page to bookmarks
		 *
		 * @param {number} page_number Page number to add.
		 */
		add_page( page_number ) {
			this.update( ( { pages, ...rest } ) => ( {
				...rest,
				pages: [ ...pages, page_number ].filter( ( v, i, a ) => a.indexOf( v ) === i ).sort(),
			} ) );
		},

		/**
		 * Remove page from bookmarks
		 *
		 * @param {number} page_number Page number to remove.
		 */
		remove_page( page_number ) {
			this.update( ( { pages, ...rest } ) => ( {
				...rest,
				pages: pages.filter( p => p !== page_number ),
			} ) );
		},
	};
}
