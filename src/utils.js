/**
 * Convert number to unicode arabic string
 *
 * @param {number} number Number to convert to unicode arabic.
 * @return {string} Unicode arabic string.
 */
export function arabic_number( number ) {
	const codes = '\u0660\u0661\u0662\u0663\u0664\u0665\u0666\u0667\u0668\u0669';

	return new String( number ).replace( /[0-9]/g, d => codes[ Number( d ) ] );
}

/**
 * Decorate ayah number
 *
 * @param {number} number Ayah number.
 * @return {string} Ayah number text.
 */
export function ayah_number( number ) {
	return '\ufd3f' + arabic_number( number ) + '\ufd3e';
}
