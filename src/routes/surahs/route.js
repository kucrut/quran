/** @typedef { import("../../types").Data } Data */
/** @typedef { import("../../types").JuzItem } JuzItem */
/** @typedef { { juz_list: JuzItem[]; }} RouteData extends Data */

const route_path = '/surahs';

/**
 * Generate route data
 *
 * @param {Object} arg      Route arguments.
 * @param {Data}   arg.data Data.
 * @return {RouteData} Route data.
 */
function generate_route_data( { data } ) {
	const { ayahs, juzs, surahs } = data;

	/** @type {number[]} */
	let added_juz_surah_numbers = [];

	const juz_list = juzs.map( ( { first_verse_id, juz_number, verse_mapping } ) => {
		const juz_surahs = Object.keys( verse_mapping )
			.map( Number )
			.filter(
				surah_number =>
					! added_juz_surah_numbers.includes( surah_number ) &&
					verse_mapping[ surah_number ].startsWith( '1' ),
			)
			.map( surah_number => surahs.find( surah => surah.id === surah_number ) );

		added_juz_surah_numbers = added_juz_surah_numbers.concat( juz_surahs.map( ( { id } ) => id ) );

		return {
			number: juz_number,
			page_number: ayahs.find( a => a.id === first_verse_id ).v1_page,
			surahs: juz_surahs,
		};
	} );

	return {
		...data,
		juz_list,
	};
}

module.exports = {
	permalink: route_path,
	all: () => [ { slug: route_path } ],
	data: generate_route_data,
};
