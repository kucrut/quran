/** @typedef { import("@elderjs/elderjs").RequestOptions } RequestOptions */
/** @typedef { import("../../types").Data } Data */
/** @typedef { { page_number: number; } } PageRequestOptions extends RequestOptions */

/**
 * Generate route data
 *
 * @param {Object}             arg         Route arguments.
 * @param {Data}               arg.data    Data.
 * @param {PageRequestOptions} arg.request Request options.
 * @return {Data} Route data.
 */
function generate_route_data( { data, request } ) {
	const { ayahs, surahs, surah_of_page } = data.pages.find( p => p.id === request.page_number );
	const page_ayahs = ayahs.map( p_a => data.ayahs.find( a => a.id === p_a ) );
	const page_surahs = surahs.map( p_s => data.surahs.find( s => s.id === p_s ) );

	return {
		...data,
		page_ayahs,
		page_surahs,
		surah_of_page,
	};
}

/**
 * Generate route params
 *
 * @param {Object} arg      Route arg.
 * @param {Data}   arg.data Data.
 * @return {Record<'page_number',number>[]} Array of all route params.
 */
function generate_route_params( { data } ) {
	return data.PAGE_NUMBERS.map( page_number => ( { page_number } ) );
}

module.exports = {
	permalink: '/page/:page_number',
	all: generate_route_params,
	data: generate_route_data,
};
