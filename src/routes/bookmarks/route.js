const page_path = '/bookmarks';

module.exports = {
	permalink: page_path,
	all: () => [ { slug: page_path } ],
};
